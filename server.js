// Main file to init the application.

// Require the express module.
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// require Customer routes.
const customerRoutes = require('./Routes/customer');
// Require loan routes.
const loanRoutes = require('./Routes/loan');
// Require repayment routes.
const rePaymentRoutes = require('./Routes/rePayment');
// Create the express Node app.
const app = express();

// parse application/json
app.use(bodyParser.json());

// Add customer routes to the application.
customerRoutes(app);
// Adding loan routes to the application.
loanRoutes(app);
//Adding rePayment rountes to the application.
rePaymentRoutes(app);
 
// Create the base route.
app.get('/', function (req, res) {
  res.send('Welcome To Instal Loan');
});
 
// Start the server.
app.listen(3000);
console.log("App is running in the URL: http://localhost:3000");


const nodemailer = require('./Utils/nodemailer')

nodemailer ("dharma.bharam@gmail.com, aarthi0789@gmail.com", "hello this is test mail ", "<h1>Welcome to Our Website</h1>" , function (err,result) {
		console.log("error, result @ nodemailer ", err,result)
}  )

// DB connection.
// mongoose.connect(MongoDB_connection_String, Options_object, callback-function);
mongoose.connect('mongodb://localhost:27017/DharmaGuruu', {useNewUrlParser: true}, function(err, res){
	if(err){
		console.log('\x1b[32m',"@ DB connection error: ", err);
	}else{
		console.log('\x1b[34m',"@ MongoDB connection established successfully.");
	}
}); 
 


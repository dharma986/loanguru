// This file carries all the customer related APIs handler methods.
const customers_model = require('../Models/customers');

// Require Utility functions.
const universalFunc = require('../Utils/universalFunctions');

module.exports = {

	/*
		methodNameForAPI: annamousFunction(dataObject, calllbackFunction);
	*/

	// Add new customer.
	addCustomer: function(data, cb){

		console.log("Controllers: Inside addCustomer function.", typeof(data.firstName)=='string');

		customers_model(data).save(function(err, res){
			if(err){
				cb(err, null);
			}else{
				let resp = {
					message: "Customer created scuccessfully.",
					savedUserObject: res,
					author: "dharma bharam",
					date: new Date()
				};
				// Send email to the register user.
				universalFunc.sendEmail(data.email, function(err, res1){
					if(err){
						cb(err, null);
					} else {
						resp.emailStatus = `email sent successfully to ${data.email}`;
						cb(null, resp);
					}
				});
			}
		});
	},

	// Get all the customers list in the DB.
	listCustomers: function(cb){

		var queryObj = {};

		customers_model.find(queryObj, function(err, res){
			if(err){
				cb(err, null);
			}else{
				cb(null, res);
			}
		});
	},

	// Update customer record.
	updateCustomer: function(data, cb){
		// Here is the logic to update the customer data which is already there in our database.
		let queryObj = {
			_id: data._id
		};
		let updateObj = {
			firstName: data.firstName,
			lastName: data.lastName,
			age: data.age,
			gender: data.gender,//, enum: ["male", "female", "others"]},
			address: data.address,
			mobile_num: data.mobile_num,
			email: data.email,
			occupation: data.occupation

		};
		customers_model.findOneAndUpdate(queryObj, updateObj, function(err, res){
			if(err){
				cb(err, null);
			}else{
				let resp = {
					message: "Customer record updated successfully.",
					data: res
				}
				cb(null, resp);
			}
		});
	},

	// Delete customer record.
	deleteCustomer: function(data, cb){
		// write the DB query to delete the customer reord from the collection.
		let queryObj = {
			_id: data.customerId
		};
		customers_model.deleteOne(queryObj, function(err, res){
			if(err){
				cb(err, null);
			}else{
				let resp = {
					message: "Customer record deleted successfully.",
					data: res
				}
				cb(null, resp);
			}
		});
	}

};














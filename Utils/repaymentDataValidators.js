const Joi = require('joi');

module.exports={

       repay_reg:Joi.object().keys({
       loanId:Joi.string().required(),
        customerId: Joi.string().required(),
        re_payment_amount:Joi.string().required(),
      

       }),
       repay_list:Joi.object().keys({
              loanId:Joi.string(),
        customerId: Joi.string(),
        re_payment_amount:Joi.string(),
      

       }),
       repay_update:Joi.object().keys({
              loanId:Joi.string().required(),
        customerId: Joi.string().required(),
        re_payment_amount:Joi.string().required(),
      

       }),
       repay_delete:Joi.object().keys({
        loanId:Joi.string().required()
        

       })

}
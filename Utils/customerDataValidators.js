// This file contains all the request objects validators Joi schemas.
 
const Joi = require('joi');

module.exports = {

	// Customer registration req data validator schema.
	customer_reg: Joi.object().keys({
		firstName: Joi.string().required().min(3).max(6),
		lastName: Joi.string().required(),
		age: Joi.string().required(), 
		gender: Joi.string().required().valid("male", "female", "others"),
		address: Joi.string().required(),
		mobile_num: Joi.string().required(),
		email: Joi.string().required(),
		occupation: Joi.string().required()
	}),

	customer_get: Joi.object().keys({
	
		firstName: Joi.string(),
		lastName: Joi.string(),
		age: Joi.string(),
		gender: Joi.string(),
		address: Joi.string(),
		mobile_num: Joi.string(),
		email: Joi.string(),
		occupation: Joi.string()

	}),
	customer_updaate: Joi.object().keys({
		_id:Joi.string().required().min(24),
		firstName: Joi.string().required().min(3).max(6),
		lastName: Joi.string().required(),
		age: Joi.string().required().min(2).max(2),
		gender: Joi.string().required().valid("male", "female", "others"),
		address: Joi.string().required(),
		mobile_num: Joi.string().required(),
		email: Joi.string().required(),
		occupation: Joi.string().required()

	}),
	customer_delete: Joi.object().keys({

		customerId:Joi.string().required().min(24)


	})


};
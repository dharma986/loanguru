const Joi = require('joi');


module.exports = {

    loan_reg: Joi.object().keys({
        loanType: Joi.string().required(),	// 3-Months, 6-Months, 12-Months
        amount: Joi.string().required(),
        customerId: Joi.string().required().min(24),
        payment_mode: Joi.string().required()


    }),
    loan_list:Joi.object().keys({

        loanType: Joi.string(),	// 3-Months, 6-Months, 12-Months
        amount: Joi.string(),
        customerId: Joi.string(),
        payment_mode: Joi.string()


    }),
    loan_update:Joi.object().keys({
        _id : Joi.string().required(),
        loanType: Joi.string().required(),	// 3-Months, 6-Months, 12-Months
        amount: Joi.string().required(),
        customerId: Joi.string().required().min(24),
        payment_mode: Joi.string().required()

    }),
    
    loan_Delete: Joi.object().keys({
        loanId : Joi.string().required().min(24)
    })

}